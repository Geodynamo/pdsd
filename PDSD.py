#!/usr/bin/env python
# coding: utf-8

#Coded by T.MAZLOUM - UGA - Earth Sciences ; 
#Supervised by N.Schaeffer - IsTerre ;
# 22 / 07 / 2021

import pandas as pd
import math

df_lst = []
labels = []
colors = []

###############################
label = 'Soderlund+ 2012'
df = pd.read_csv('soderlund_2012.txt', engine='python', delimiter="\s+", comment='%', header=None,
    names= ['E','Pm','Ra','Ra/RaC','N_{ro}','l_{max}','Nu','Re_{c}','Re','C_omega_z','|H_z^rel|',
        'k_{u}','k_{B}','f','Lambda_{i}','Lambda{d}','lambda','frac{F_L}{F_C}','frac{F_I}{F_V}'])

#Adding missing variables :
df["E"] *= 2 #Correcting Ekman's number by removing the 2 factor;
df["Pr"]=1
df["Ro"]=df["Re"]*df["E"]
df["Rm"]=df["Re"]*df["Pm"]
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#07c98c")

###############################
label = 'Christensen+ 2006'
df = pd.read_csv('christensen_aubert_2006_scaling.data', engine='python', delimiter = "\s+", comment='%', header=None,
    names=['E','Ra', 'Rac', 'Pr', 'Pm', 'icc', 'lmax', 'nr', 'mc', 'Re', 'Rm', 'Ro',
       'Ekin', '%po_U', '%ta_U', 'lm_U', 'mmd_U', 'Nu',
       'Emag', '%ex', '%po_B', '%pa_B', '%ta_B', 'lm_B', 'mmd_B', 
       'B', 'Bsur', 'Bdip', 'pow', '%jou', 'tmag', 'tvis', 'trun', 'tad', 'Tm', 'T2', 'vrat'])

#Adding missing variables :
df["Ro"] *= 1e-3     #Correcting Rossby Number by removing the 10^3 factor according to the datafile;
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#07a5c9")

###############################
label = 'Takahashi+ 2008'
df = pd.read_csv('takahashi2008_data.txt', engine='python', delimiter = "\s+", comment='%', header=None,
    names=['E','Ra','Pm','Ek','Em','Rm','Re','Elsass.','Nu','fdip','nr','lmax'])

#Adding missing variables :
df["Pr"]=1
df["Ro"]=df["Re"]*df["E"]
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#c907a5")

###############################
label = 'Yadav+ 2016b'
df = pd.read_csv('yadav_2016b_geodynamo_boussinesq.txt', engine='python', delimiter = "\s+", comment='#', header=None,
    names=['Ra','E','Pm','KE_pol','KE_tor','Re','Rol','Nu','ME_pol','ME_tor','Elsasser','Elsasser_CMB','Dip','Dip_CMB','Dip_CMB_l11','Buo_pow','Ohm_diss','l_bar_u','l_bar_B','run_time','l_max','r_max'])

#Adding missing variables :
df["Pr"]=1
df["Ro"]=df["Re"]*df["E"]
df["Rm"]=df["Re"]*df["Pm"]
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#f9ce45")

###############################
label = 'Yadav+ 2016a (stress-free)'
df = pd.read_csv('yadav_stress-free_dynamo.data', engine='python', delimiter = "\s+", comment='%', header=None,
    names=['Ra','E','Pr','Pm','Nrho','Ekin_pol','Ekin_tor','Ekin_pola','Ekin_tora','U1','U2','U3','U4',
         'Emag_pol','Emag_tor','Emag_pola','Emag_tora','Rm','U5','Rol','U6','Dip','Dip_CMB','Els','Els_CMB', 
         'Nu','dlV','U7','dlB','dmB','U8','U9','U10','U11','U12','U13','U14','U15','U16','U17','U18','U19','U20'])

#Adding missing variables :
df["Re"]=df["Rm"]/df["Pm"]
df["Ro"]=df["Re"]*df["E"]
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#f9ce45")

###############################
label = 'Yadav+ 2016a (no-slip)'
df = pd.read_csv('yadav_no-slip_dynamo.data', engine='python', delimiter = "\s+", comment='%', header=None,
    names=['Ra','E','Pr','Pm','Nrho','Ekin_pol','Ekin_tor','Ekin_pola','Ekin_tora','U1','U2','U3','U4',
         'Emag_pol','Emag_tor','Emag_pola','Emag_tora','Rm','U5','Rol','U6','Dip','Dip_CMB','Els','Els_CMB',
         'Nu','dlV','U7','dlB','dmB','U8','U9','U10','U11','U12','U13','U14','U15','U16','U17','U18','U19','U20'])

#Adding missing variables :
df["Re"]=df["Rm"]/df["Pm"]
df["Ro"]=df["Re"]*df["E"]
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#f9ce45")

###############################
label = 'Schwaiger+ 2019'
df = pd.read_csv('Schwaiger_2019.txt', engine='python', delimiter = "\s+", comment='#', header=None,
    names=['E','Ra','Pm','Nu','Rm','Els','M','delta','chi_norm','f_ohm','f_dip','l_pol','l_MA','l_MS','l_CA2006','L_ohm',
         'Brms_cmb','Bl1_cmb','buo_power'])

#Adding missing variables :
df["Pr"]=1 #according to the source article ("Pr=1")
df["Re"]=df["Rm"]/df["Pm"]
df["Ro"]=df["Re"]*df["E"]
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#eb7d34")

###############################
label = 'Cebron+ 2019'
df = pd.read_csv('db_precession_CLNS2019.txt', engine='python', delimiter = "\s+", comment='%', header=None,
    names=['visc','Po','angle','ri','ro','Pm','Es','Ea','EtorS','Em','Bsurf_dip','f_dip','dOmega','theta','phi',
        'Dnu','Deta','trun','Sconv_U','Sconv_B','class','NR','Lmax','Mmax','hyper_nu','frame','condIC'])

#Adding missing variables :
df["E"]=df["visc"]/(df["ro"]**2 * (1+df["Po"])) #According to the paper and the Ipynb file.
df["Pr"]=1
df["Vol"]=((4/3)*(math.pi)*((df["ro"]**3)-(df["ri"]**3)))
df["Ro"]=(((4*(df["Ea"]))/(df["Vol"]))**0.5)
df["Re"]=(df["Ro"])/(df["E"])
df["Rm"]=(df["Re"])*(df["Pm"])
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#043ff8")

###############################
label = 'Lin+ 2016'
df = pd.read_csv('Yufeng_Lin_fullsphere_dynamo_db.txt', engine='python', delimiter = "\s+", comment='%', header=None,
    names=['E','Po','angle','ri/ro','Pm','tmag','class','Nr','Lmax','Mmax'])

#Adding missing variables :
#ri = 0 et ro =1, fixed standard values for this simulation 
df["ri"]=0
df["ro"]=1
df["Pr"]=1
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#000000")

###############################
label = 'Schaeffer+ 2017'
df = pd.read_csv('schaeffer_2017.txt', engine='python', delimiter = "\s+", comment='#')
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#00aa00")

###############################
label = 'Tassin+ 2021'      ## colums: 'ek', 'pr', 'sc', 'ra', 'raxi', 'prmag', 'rm', 'rossby_l', 'elsasser', 'fohm', 'chi', 'ekin_esnz_rel', 'dip_l11', 'buoPower_temp_rel','deltaTnuss', 'deltasherwood'
df = pd.read_json('tassin_2021_double-diff.json')
df.rename(columns = {'ek':'E', 'pr':'Pr', 'sc':'Sc', 'ra':'Ra', 'prmag':'Pm', 'rm':'Rm'}, inplace = True)
df['Ro']=df['Rm']/df['Pm']*df['E']
df["Origin"]=label
labels.append(label)
df_lst.append(df)
colors.append("#ff0000")


###############################
label = 'Earth'
df = pd.DataFrame({'E':[1e-15,], 'Pm':[1e-6,], 'Ro':[1e-6,], 'Origin':[label,]})
#Adding missing variables :
df["Pr"]=1 #Supposing Pr's value is unity...
df["Re"]=(df["Ro"])/(df["E"])
df["Rm"]=df["Re"]*df["Pm"]
labels.append(label)
df_lst.append(df)
colors.append("#00ff00")



a=pd.concat(df_lst, ignore_index=True)
a["size"] = (a["Pr"]**0.5)*10



from bokeh.plotting import figure
from bokeh.io import output_file, output_notebook
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource
from bokeh.layouts import row, column, gridplot
from bokeh.models import CDSView, ColumnDataSource, GroupFilter
from bokeh.models import Circle
from bokeh.models import Legend
from bokeh.models.widgets import Div
from bokeh.models import Label, LabelSet, Range1d


## Keep only the columns we use, to reduce size of html
a = a.filter(['E','Pm','Rm','Ro','Ra','Pr','Re','Origin','size'], axis=1)
source = ColumnDataSource(data=a)


# In[9]:


output_file('filename.html', title="P.D.S.D - Planetary Dynamo Simulation Database")  # Render to static HTML, or
#output_notebook()  # Render inline in a Jupyter Notebook

TOOLTIPS = [
    ("E", "@E"),
    ("Ra", "@Ra"),
    ("Pr", "@Pr"),
    ("Ro", "@Ro"),
    ("Pm", "@Pm"),
    ("Re", "@Re"),
    ("Rm", "@Rm"), 
    ("Origin", "@Origin")]

TOOLS = "pan,wheel_zoom,box_zoom,reset,box_select,lasso_select,help,tap"


#creating different filters :
views = []
for origin in labels:
    views.append( CDSView(filter=GroupFilter(column_name='Origin', group = origin)) )

#renderer fucntion is to change glyph color when hovering over a selected point(or points);

#Graph 1, x=E and y=Pm :

p_fil=figure(title='Ekman number (E) vs magnetic Prandtl number (Pm)', width=740, height=520,tools=TOOLS,
            x_axis_type="log",y_axis_type="log", tooltips=TOOLTIPS, x_range=(1e-17, 1e-1), y_range=(1e-7, 1e2))

p_fil.title.align = "center"
p_fil.title.text_font_style = "italic"

renderers_left = [] 
leg_items = []     
for i in range(len(views)):
    renderer= p_fil.circle(x='E', y='Pm', fill_alpha=0.35, line_color=colors[i], line_alpha=0.5, size='size', 
             source=source, color=colors[i], view=views[i])

    selected_circle = Circle(fill_alpha=0.8, fill_color="firebrick", line_color=None)
    renderer.selection_glyph = selected_circle
    renderers_left.append(renderer)
    leg_items.append( (labels[i], [renderer]) )


legend = Legend(items=leg_items, location="center")

#Legend :
p_fil.add_layout(legend, 'right')
p_fil.legend.click_policy="hide"
p_fil.legend.label_text_font_size = "7pt"
p_fil.legend.title = 'Click to hide a database'


#x axis
p_fil.xaxis.axis_label = "Ekman number (E)"

#y axis
p_fil.yaxis.axis_label = "Magnetic Prandtl number (Pm)"



#Graph 2, x=Rm and y=Ro :

p_fil2=figure(title='Rossby number (Ro) vs magnetic Reynolds number (Rm)', width=740, height=520, tools=TOOLS,
                x_axis_type="log",y_axis_type="log", tooltips=TOOLTIPS, x_range=(1, 1e5), y_range=(5e-7, 2))

p_fil2.title.align = "center"
p_fil2.title.text_font_style = "italic"

leg2_items = []
for i in range(len(views)):
    renderer = p_fil2.circle(x='Rm', y='Ro', fill_alpha=0.35, line_color=colors[i], line_alpha=0.5, size='size', 
             source=source, color=colors[i], view=views[i])

    selected_circle = Circle(fill_alpha=0.8, fill_color="firebrick", line_color=None)
    renderer.selection_glyph = selected_circle
    leg2_items.append( (labels[i], [renderer]) )

legend = Legend(items=leg2_items, location="center")

#Legend
p_fil2.add_layout(legend, 'right')
p_fil2.legend.click_policy="hide"
p_fil2.legend.label_text_font_size = "7pt"
p_fil2.legend.title = 'Click to hide a database'


#x axis
p_fil2.xaxis.axis_label = "Magnetic Reynolds number (Rm)"

#y axis
p_fil2.yaxis.axis_label = "Rossby number (Ro)"



#Annotations & Labels :

label6 = Label(x=420, y=455, x_units='screen', y_units='screen',
                 text='Interactive Toolbar',
                 text_alpha=0.5, text_font_size="10pt", text_font_style="bold italic")

div = Div(text="""
<h2>Links to database source articles:</h2>
<p><ul><li>
Cebron+ 2019 (Precession): <a href="https://doi.org/10.1093/gji/ggz037">https://doi.org/10.1093/gji/ggz037</a></li><li>
Christensen+ 2006: <a href="https://doi.org/10.1111/j.1365-246X.2006.03009.x">https://doi.org/10.1111/j.1365-246X.2006.03009.x</a></li><li>
Lin+ 2016 (Precession): <a href="https://doi.org/10.1063/1.4954295">https://doi.org/10.1063/1.4954295</a></li><li>
Schaeffer+ 2017:  <a href="https://doi.org/10.1093/gji/ggx265">https://doi.org/10.1093/gji/ggx265</a></li><li>
Schwaiger+ 2019: <a href="https://doi.org/10.1093/gji/ggz192">https://doi.org/10.1093/gji/ggz192</a></li><li>
Soderlund+ 2012: <a href="https://doi.org/10.1016/j.epsl.2012.03.038">https://doi.org/10.1016/j.epsl.2012.03.038</a></li><li>
Takahashi+ 2008: <a href="https://doi.org/10.1016/j.pepi.2008.03.005">https://doi.org/10.1016/j.pepi.2008.03.005</a></li><li>
Tassin+ 2021 (Double-diffusive):  <a href="https://doi.org/10.1093/gji/ggab161">https://doi.org/10.1093/gji/ggab161</a></li><li>
Yadav+ 2016a: <a href="https://doi.org/10.1093/gji/ggv506">https://doi.org/10.1093/gji/ggv506</a></li><li>
Yadav+ 2016b: <a href="https://doi.org/10.1073/pnas.1608998113">https://doi.org/10.1073/pnas.1608998113</a>
</li></ul>
</p>
<p align=center>
    Developed by Toufic Mazloum - UGA (Grenoble Alpes University) - Department of Earth Sciences, under the supervision of Prof. Nathanael Schaeffer - ISTerre/CNRS<br/>
    nathanael.schaeffer@univ-grenoble-alpes.fr<br/>
    Contact us in order to include your database : nathanael.schaeffer@univ-grenoble-alpes.fr  toufic.mazloum@etu.univ-grenoble-alpes.fr<br/>
    Source code available: <a href="https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pdsd">https://gricad-gitlab.univ-grenoble-alpes.fr/Geodynamo/pdsd</a>
<p>
""")

p_fil2.add_layout(label6)

#Final : 
res=(gridplot([[p_fil, p_fil2]]))
show(column(Div(text=
    """<h1>P.D.S.D - Planetary Dynamo Simulation Database (beta)</h1><h2>A Comparison between different simulation data based on Ekman's number (E), the magnetic Prandtl number (Pm), the magnetic Reynolds number (Rm) and Rossby's number (Ro) : </h2><h> Use "Tap" tool from the interactive toolbar to select a point and see which are the corresponding ones on both graphs. <br/>All sources are listed below.</h>"""
    ), res,  div))
